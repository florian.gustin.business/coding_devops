const express = require('express');
const {Client} = require('node-postgres');
const app = express();
const cors = require('cors');

const PORT = 3001;

app.use(cors());

(async () => {
    const client = new Client({
        host: 'psql',
        user: 'myuser',
        database: 'mydatabase',
        password: 'mypassword',
        // user: process.env.PG_USR,
        // database: process.env.PG_DB,
        // password: process.env.PG_PWD,
        port: 5432
    });
    await client.connect();
    await client.query('CREATE TABLE IF NOT EXISTS MESSAGES (ID INT PRIMARY KEY NOT NULL, DATASTRING TEXT NOT NULL);');
    await client.query('INSERT INTO MESSAGES (ID, DATASTRING) VALUES(1, \'Hi my loulou !\')');
    await client.query('INSERT INTO MESSAGES (ID, DATASTRING) VALUES(2, \'Es-tu en forme ?\')');
    await client.end();
})().catch(console.error);


app.get('/', (request, response) => {
    response.send('Welcome to Node Js web server !');

});

app.get('/test', (request, response) => {
    (async () => {
        const client = new Client({
            host: 'psql',
            user: 'myuser',
            database: 'mydatabase',
            password: 'mypassword',
            // user: process.env.PG_USR,
            // database: process.env.PG_DB,
            // password: process.env.PG_PWD,
            port: 5432
        });
        await client.connect();
        const data = await client.query('SELECT * FROM MESSAGES');
        await client.end();
        response.send(`${JSON.stringify(data.rows)}`);
    })().catch(console.error);
});

app.listen(PORT, () => {
    console.log(`Node server running and listening on port ${PORT}`);
});
